public class Programa {
    public static void main(String[] args) {
       
        Ponto ponto1 = new Ponto();

       
        Ponto ponto2 = new Ponto(2, 5);

       
        double distancia1Para2 = ponto1.calcularDistancia(ponto2);
        System.out.println("Distância do ponto1 ao ponto2: " + distancia1Para2);

        
        double distancia2ParaCoordenadas = ponto2.calcularDistancia(7, 2);
        System.out.println("Distância do ponto2 às coordenadas (7, 2): " + distancia2ParaCoordenadas);

        
        ponto1.setX(10);

        
        ponto1.setY(3);

        
        System.out.println("Ponto1 atualizado: (" + ponto1.getX() + ", " + ponto1.getY() + ")");
    }
}