package br.edu.up.view;

import java.util.List;
import br.edu.up.model.Disciplina;

public class AlunoView {
    public void imprimirDetalhesAluno(String nome, String rg, String matricula, int anoIngresso, String curso, String turno) {
        System.out.println("Detalhes do Aluno:");
        System.out.println("Nome: " + nome);
        System.out.println("RG: " + rg);
        System.out.println("Matrícula: " + matricula);
        System.out.println("Ano de Ingresso: " + anoIngresso);
        System.out.println("Curso: " + curso);
        System.out.println("Turno: " + turno);
    }

    public void imprimirDisciplinas(List<Disciplina> disciplinas) {
        System.out.println("Disciplinas Matriculadas:");
        for (Disciplina disciplina : disciplinas) {
            System.out.println("Disciplina: " + disciplina.getNome() + ", Identificador: " + disciplina.getIdentificador());
        }
    }
}
