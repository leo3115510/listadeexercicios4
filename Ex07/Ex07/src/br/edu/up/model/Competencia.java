package br.edu.up.model;

public class Competencia {
    private String descricao;
    private boolean necessaria;
    private boolean atingida;

    public Competencia(String descricao, boolean necessaria) {
        this.descricao = descricao;
        this.necessaria = necessaria;
        this.atingida = false;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setNecessaria(boolean necessaria) {
        this.necessaria = necessaria;
    }

    public boolean isNecessaria() {
        return necessaria;
    }

    public boolean isAtingida() {
        return atingida;
    }

    public void setAtingida(boolean atingida) {
        this.atingida = atingida;
    }
}
