package br.edu.up.model;

public enum Situacao {
    APROVADO, REPROVADO, PENDENTE
}
