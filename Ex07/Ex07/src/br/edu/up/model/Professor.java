package br.edu.up.model;

public class Professor extends Pessoa {
    private String numeroCurriculoLattes;
    private String titulacao;

    public Professor(String nome, String rg, String matricula, String numeroCurriculoLattes, String titulacao) {
        super(nome, rg, matricula);
        this.numeroCurriculoLattes = numeroCurriculoLattes;
        this.titulacao = titulacao;
    }

    public String getNumeroCurriculoLattes() {
        return numeroCurriculoLattes;
    }

    public void setNumeroCurriculoLattes(String numeroCurriculoLattes) {
        this.numeroCurriculoLattes = numeroCurriculoLattes;
    }

    public String getTitulacao() {
        return titulacao;
    }

    public void setTitulacao(String titulacao) {
        this.titulacao = titulacao;
    }


}
