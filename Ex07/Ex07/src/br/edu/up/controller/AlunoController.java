package br.edu.up.controller;

import br.edu.up.model.Aluno;
import br.edu.up.model.Disciplina;
import br.edu.up.model.Situacao;
import br.edu.up.view.AlunoView;

import java.util.Scanner;

public class AlunoController {
    private Aluno model;
    private AlunoView view;
    private Scanner scanner;

    public AlunoController(Aluno model, AlunoView view) {
        this.model = model;
        this.view = view;
        this.scanner = new Scanner(System.in);
    }

    public void atualizarView() {
        view.imprimirDetalhesAluno(model.getNome(), model.getRg(), model.getMatricula(), model.getAnoIngresso(), model.getCurso(), model.getTurno());
    }

    public void setNome() {
        System.out.print("Digite o nome: ");
        String nome = scanner.nextLine();
        model.setNome(nome);
    }

    public void setRg() {
        System.out.print("Digite o RG: ");
        String rg = scanner.nextLine();
        model.setRg(rg);
    }

    public void setMatricula() {
        System.out.print("Digite a matrícula: ");
        String matricula = scanner.nextLine();
        model.setMatricula(matricula);
    }

    public void setAnoIngresso() {
        System.out.print("Digite o ano de ingresso: ");
        int anoIngresso = scanner.nextInt();
        scanner.nextLine(); 
        model.setAnoIngresso(anoIngresso);
    }

    public void setCurso() {
        System.out.print("Digite o curso: ");
        String curso = scanner.nextLine();
        model.setCurso(curso);
    }

    public void setTurno() {
        System.out.print("Digite o turno: ");
        String turno = scanner.nextLine();
        model.setTurno(turno);
    }

    public void adicionarDisciplina(Disciplina disciplina) {
        model.adicionarDisciplina(disciplina);
    }

    public void removerDisciplina(Disciplina disciplina) {
        model.removerDisciplina(disciplina);
    }

    public void listarDisciplinas() {
        view.imprimirDisciplinas(model.getDisciplinas());
    }

    public Situacao avaliarSituacao() {
        int competenciasNecessariasAtingidas = 0;
        int competenciasComplementaresAtingidas = 0;
        int totalCompetenciasNecessarias = 0;
        int totalCompetenciasComplementares = 0;

        for (Disciplina disciplina : model.getDisciplinas()) {
            totalCompetenciasNecessarias += disciplina.getCompetencias().stream().filter(c -> c.isNecessaria()).count();
            totalCompetenciasComplementares += disciplina.getCompetencias().stream().filter(c -> !c.isNecessaria()).count();

            competenciasNecessariasAtingidas += disciplina.getCompetencias().stream().filter(c -> c.isNecessaria() && c.isAtingida()).count();
            competenciasComplementaresAtingidas += disciplina.getCompetencias().stream().filter(c -> !c.isNecessaria() && c.isAtingida()).count();
        }

        if (competenciasNecessariasAtingidas == totalCompetenciasNecessarias && competenciasComplementaresAtingidas >= totalCompetenciasComplementares / 2) {
            return Situacao.APROVADO;
        } else if (competenciasNecessariasAtingidas < totalCompetenciasNecessarias / 2 || competenciasComplementaresAtingidas < totalCompetenciasComplementares / 2) {
            return Situacao.REPROVADO;
        } else {
            return Situacao.PENDENTE;
        }
    }
}
