import br.edu.up.controller.AlunoController;
import br.edu.up.model.Aluno;
import br.edu.up.view.AlunoView;

public class Main {
    public static void main(String[] args) {
        Aluno aluno = new Aluno("", "", "", 0, "", "");
        AlunoView view = new AlunoView();
        AlunoController controller = new AlunoController(aluno, view);

        controller.setNome();
        controller.setRg();
        controller.setMatricula();
        controller.setAnoIngresso();
        controller.setCurso();
        controller.setTurno();

        controller.atualizarView();
    }
}
