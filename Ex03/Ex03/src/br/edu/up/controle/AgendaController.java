package br.edu.up.controle;

import br.edu.up.modelo.Agenda;
import br.edu.up.modelo.Compromisso;
import br.edu.up.tela.AgendaView;

public class AgendaController {
    private Agenda agenda;
    private AgendaView view;

    public AgendaController(Agenda agenda, AgendaView view) {
        this.agenda = agenda;
        this.view = view;
    }

    public void executar() {
        String opcao;
        do {
            view.mostrarMenu();
            opcao = view.obterEntrada();
            switch (opcao) {
                case "1":
                    Compromisso compromisso = view.obterDetalhesCompromisso();
                    agenda.adicionarCompromisso(compromisso);
                    break;
                case "2":
                    view.mostrarCompromissos(agenda.getCompromissos());
                    break;
                case "3":
                    System.out.println("Saindo...");
                    break;
                default:
                    System.out.println("Opção inválida. Tente novamente.");
            }
        } while (!opcao.equals("3"));
    }
}
