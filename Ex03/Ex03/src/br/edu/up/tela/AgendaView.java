package br.edu.up.tela;

import java.util.List;
import java.util.Scanner;

import br.edu.up.modelo.Compromisso;

public class AgendaView {
    private Scanner scanner;

    public AgendaView() {
        scanner = new Scanner(System.in);
    }

    public void mostrarMenu() {
        System.out.println("1. Adicionar compromisso");
        System.out.println("2. Listar compromissos");
        System.out.println("3. Sair");
    }

    public String obterEntrada() {
        return scanner.nextLine();
    }

    public Compromisso obterDetalhesCompromisso() {
        System.out.print("Pessoa: ");
        String pessoa = scanner.nextLine();
        System.out.print("Local: ");
        String local = scanner.nextLine();
        System.out.print("Horário: ");
        String horario = scanner.nextLine();
        System.out.print("Assunto: ");
        String assunto = scanner.nextLine();
        System.out.print("Data: ");
        String data = scanner.nextLine();
        return new Compromisso(pessoa, local, horario, assunto, data);
    }

    public void mostrarCompromissos(List<Compromisso> compromissos) {
        System.out.println("Compromissos:");
        for (Compromisso compromisso : compromissos) {
            System.out.println(compromisso.getPessoa() + " - " + compromisso.getLocal() +
                    " - " + compromisso.getHorario() + " - " + compromisso.getAssunto() +
                    " - " + compromisso.getData());
        }
    }
}
