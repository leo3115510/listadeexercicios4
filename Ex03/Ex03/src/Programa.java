import br.edu.up.controle.AgendaController;
import br.edu.up.modelo.Agenda;
import br.edu.up.tela.AgendaView;

public class Programa {
    public static void main(String[] args) {
        Agenda agenda = new Agenda();
        AgendaView view = new AgendaView();
        AgendaController controller = new AgendaController(agenda, view);
        controller.executar();
    }
}
