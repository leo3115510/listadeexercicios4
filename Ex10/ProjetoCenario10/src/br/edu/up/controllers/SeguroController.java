package br.edu.up.controllers;

import java.util.ArrayList;
import java.util.List;
import br.edu.up.models.Seguro;

public class SeguroController {

    private List<Seguro> seguros;

    public SeguroController() {
        seguros = new ArrayList<>();
    }

    public boolean adicionarSeguro(Seguro seguro) {
        if (consultarSeguro(seguro.getNumeroApolice()) != null) {
            return false;
        }
        seguros.add(seguro);
        return true;
    }

    public Seguro consultarSeguro(int numeroApolice) {
        for (Seguro seguro : seguros) {
            if (seguro.getNumeroApolice() == numeroApolice) {
                return seguro;
            }
        }
        return null;
    }

    public boolean excluirSeguro(int numeroApolice) {
        Seguro seguro = consultarSeguro(numeroApolice);
        if (seguro != null) {
            seguros.remove(seguro);
            return true;
        }
        return false;
    }

    public void excluirTodosSeguros() {
        seguros.clear();
    }

    public int quantidadeSeguros() {
        return seguros.size();
    }

    public List<Seguro> listarSeguros() {
        return new ArrayList<>(seguros);
    }
}
