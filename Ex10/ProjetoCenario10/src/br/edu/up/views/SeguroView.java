package br.edu.up.views;

import java.util.List;
import br.edu.up.models.Prompt;
import br.edu.up.models.Seguro;
import br.edu.up.models.SeguroVeiculo;
import br.edu.up.models.SeguroVida;

public class SeguroView {

    public int exibirMenu() {

        Prompt.imprimir("Menu de Apólices de Seguro");
        Prompt.imprimir("1. Incluir seguro");
        Prompt.imprimir("2. Localizar seguro");
        Prompt.imprimir("3. Excluir seguro");
        Prompt.imprimir("4. Excluir todos os seguros");
        Prompt.imprimir("5. Listar todos os seguros");
        Prompt.imprimir("6. Ver quantidade de seguros");
        Prompt.imprimir("7. Sair");
        return Prompt.lerInteiro("Escolha uma opção: ");
    }

    public SeguroVida lerSeguroVida(int numeroApolice) {

        String titular = Prompt.lerLinha("Titular: ");
        double valorSegurado = Prompt.lerDecimal("Valor Segurado: ");
        return new SeguroVida(numeroApolice, titular, valorSegurado);
    }

    public SeguroVeiculo lerSeguroVeiculo(int numeroApolice) {

        String titular = Prompt.lerLinha("Titular: ");
        String modeloVeiculo = Prompt.lerLinha("Modelo do Veículo: ");
        return new SeguroVeiculo(numeroApolice, titular, modeloVeiculo);
    }

    public int lerNumeroApolice() {

        return Prompt.lerInteiro("Número da Apólice: ");
    }

    public void exibirSeguro(Seguro seguro) {

        if (seguro == null) {
            Prompt.imprimir("Seguro não encontrado.");
        } else {
            Prompt.imprimir(seguro);
        }
    }

    public void exibirSeguros(List<Seguro> seguros) {

        if (seguros.isEmpty()) {
            Prompt.imprimir("Nenhum seguro encontrado.");
        } else {
            seguros.forEach(Prompt::imprimir);
        }
    }

    public void exibirMensagem(String mensagem) {
        
        Prompt.imprimir(mensagem);
    }
}
