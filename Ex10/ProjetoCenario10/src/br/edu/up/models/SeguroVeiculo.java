package br.edu.up.models;

public class SeguroVeiculo extends Seguro {
    
    private String modeloVeiculo;

    public SeguroVeiculo(int numeroApolice, String titular, String modeloVeiculo) {
        super(numeroApolice, titular);
        this.modeloVeiculo = modeloVeiculo;
    }

    public String getModeloVeiculo() {
        return modeloVeiculo;
    }

    public void setModeloVeiculo(String modeloVeiculo) {
        this.modeloVeiculo = modeloVeiculo;
    }

    @Override
    public String toString() {
        return "SeguroVeiculo{" +
                "numeroApolice=" + getNumeroApolice() +
                ", titular='" + getTitular() + '\'' +
                ", modeloVeiculo='" + modeloVeiculo + '\'' +
                '}';
    }
}
