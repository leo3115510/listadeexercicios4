package br.edu.up.models;

public abstract class Seguro {
    
    private int numeroApolice;
    private String titular;

    public Seguro(int numeroApolice, String titular) {
        this.numeroApolice = numeroApolice;
        this.titular = titular;
    }

    public int getNumeroApolice() {
        return numeroApolice;
    }

    public void setNumeroApolice(int numeroApolice) {
        this.numeroApolice = numeroApolice;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    @Override
    public String toString() {
        return "Seguro{" +
                "numeroApolice=" + numeroApolice +
                ", titular='" + titular + '\'' +
                '}';
    }
}
