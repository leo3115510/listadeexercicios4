package br.edu.up.models;

public class SeguroVida extends Seguro {
    
    private double valorSegurado;

    public SeguroVida(int numeroApolice, String titular, double valorSegurado) {
        super(numeroApolice, titular);
        this.valorSegurado = valorSegurado;
    }

    public double getValorSegurado() {
        return valorSegurado;
    }

    public void setValorSegurado(double valorSegurado) {
        this.valorSegurado = valorSegurado;
    }

    @Override
    public String toString() {
        return "SeguroVida{" +
                "numeroApolice=" + getNumeroApolice() +
                ", titular='" + getTitular() + '\'' +
                ", valorSegurado=" + valorSegurado +
                '}';
    }
}

