import br.edu.up.controllers.SeguroController;
import br.edu.up.models.Prompt;
import br.edu.up.models.Seguro;
import br.edu.up.models.SeguroVeiculo;
import br.edu.up.models.SeguroVida;
import br.edu.up.views.SeguroView;

public class Programa {
    public static void main(String[] args) {

        SeguroController controller = new SeguroController();
        SeguroView view = new SeguroView();

        boolean executando = true;

        while (executando) {
            int opcao = view.exibirMenu();

            switch (opcao) {
                case 1:
                    int numeroApolice = view.lerNumeroApolice();

                    if (controller.consultarSeguro(numeroApolice) != null) {
                        
                        view.exibirMensagem("Número da apólice já cadastrado.");

                    } else {

                        int tipoSeguro = Prompt
                                .lerInteiro("1. Seguro de Vida\n2. Seguro de Veículo\nEscolha o tipo de seguro: ");
                        if (tipoSeguro == 1) {

                            SeguroVida seguroVida = view.lerSeguroVida(numeroApolice);
                            controller.adicionarSeguro(seguroVida);

                        } else if (tipoSeguro == 2) {

                            SeguroVeiculo seguroVeiculo = view.lerSeguroVeiculo(numeroApolice);
                            controller.adicionarSeguro(seguroVeiculo);
                        } else {

                            view.exibirMensagem("Opção inválida.");
                        }
                        
                        view.exibirMensagem("Seguro adicionado com sucesso.");
                    }
                    break;
                case 2:
                    numeroApolice = view.lerNumeroApolice();
                    Seguro seguro = controller.consultarSeguro(numeroApolice);
                    view.exibirSeguro(seguro);
                    break;
                case 3:
                    numeroApolice = view.lerNumeroApolice();
                    if (controller.excluirSeguro(numeroApolice)) {
                        view.exibirMensagem("Seguro excluído com sucesso.");
                    } else {
                        view.exibirMensagem("Seguro não encontrado.");
                    }
                    break;
                case 4:
                    Prompt.imprimir("Tem certeza que deseja excluir todos os seguros? (S/N)");
                    char confirmacao = Prompt.lerCaractere();
                    if (confirmacao == 'S' || confirmacao == 's') {
                        controller.excluirTodosSeguros();
                        view.exibirMensagem("Todos os seguros foram excluídos.");
                    } else {
                        view.exibirMensagem("Operação cancelada.");
                    }
                    break;
                case 5:
                    view.exibirSeguros(controller.listarSeguros());
                    break;
                case 6:
                    int quantidade = controller.quantidadeSeguros();
                    view.exibirMensagem("Quantidade de seguros: " + quantidade);
                    break;
                case 7:
                    executando = false;
                    view.exibirMensagem("Saindo do programa.");
                    break;
                default:
                    view.exibirMensagem("Opção inválida.");
                    break;
            }
        }
    }
}
