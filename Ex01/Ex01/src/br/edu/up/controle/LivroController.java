package br.edu.up.controle;
import br.edu.up.modelo.Livro;
import br.edu.up.tela.LivroView;

public class LivroController {
    private Livro model;
    private LivroView view;

    public LivroController(Livro model, LivroView view) {
        this.model = model;
        this.view = view;
    }

    public void atualizarView() {
        view.mostrarDetalhes(model.getCodigo(), model.getTitulo(), model.getAutores(), model.getIsbn(), model.getAno());
    }
}
