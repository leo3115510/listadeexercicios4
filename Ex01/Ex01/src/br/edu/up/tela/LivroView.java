package br.edu.up.tela;



public class LivroView {
    public void mostrarDetalhes(String codigo, String titulo, String autores, String isbn, int ano) {
        System.out.println("Detalhes do livro:");
        System.out.println("Código: " + codigo);
        System.out.println("Título: " + titulo);
        System.out.println("Autores: " + autores);
        System.out.println("ISBN: " + isbn);
        System.out.println("Ano: " + ano);
    }
}