import br.edu.up.modelo.Livro;
import br.edu.up.tela.LivroView;
import br.edu.up.controle.LivroController;

public class Programa {
    public static void main(String[] args) {
        Livro livro1 = new Livro("1598FHK", "Core Java 2", "Cay S. Horstmann e Gary Cornell", "0130819336", 2005);
        Livro livro2 = new Livro("9865PLO", "Java, Como programar", "Harvey Deitel", "0130341517", 2015);

        LivroView livroView1 = new LivroView();
        LivroController livroController1 = new LivroController(livro1, livroView1);

        LivroView livroView2 = new LivroView();
        LivroController livroController2 = new LivroController(livro2, livroView2);

        livroController1.atualizarView();
        livroController2.atualizarView();
    }
}