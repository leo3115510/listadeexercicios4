package br.edu.up.tela;

import java.util.List;

import br.edu.up.modelo.Evento;
import br.edu.up.modelo.Reserva;

public class View {
    public void exibirMenu() {
        System.out.println("1. Adicionar Evento");
        System.out.println("2. Listar Eventos");
        System.out.println("3. Adicionar Reserva");
        System.out.println("4. Listar Reservas");
        System.out.println("0. Sair");
    }

    public void listarEventos(List<Evento> eventos) {
        for (Evento evento : eventos) {
            System.out.println("Nome: " + evento.getNome() + ", Data: " + evento.getData() + ", Local: " + evento.getLocal() +
                               ", Lotação Máxima: " + evento.getLotacaoMaxima() + ", Ingressos Vendidos: " + evento.getIngressosVendidos() +
                               ", Preço do Ingresso: " + evento.getPreco());
        }
    }

    public void listarReservas(List<Reserva> reservas) {
        for (Reserva reserva : reservas) {
            System.out.println("Evento: " + reserva.getEvento().getNome() + ", Responsável: " + reserva.getResponsavel() +
                               ", Quantidade de Pessoas: " + reserva.getQuantidadePessoas() + ", Data da Reserva: " + reserva.getDataReserva() +
                               ", Valor Total: " + reserva.getValorTotal());
        }
    }
}
