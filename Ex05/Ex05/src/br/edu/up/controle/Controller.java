package br.edu.up.controle;

import java.util.ArrayList;
import java.util.List;

import br.edu.up.modelo.Evento;
import br.edu.up.modelo.Reserva;

public class Controller {
    private List<Evento> eventos;
    private List<Reserva> reservas;

    public Controller() {
        this.eventos = new ArrayList<>();
        this.reservas = new ArrayList<>();
    }

    public void adicionarEvento(String nome, String data, String local, int lotacaoMaxima, int ingressosVendidos, double preco) {
        Evento evento = new Evento(nome, data, local, lotacaoMaxima, ingressosVendidos, preco);
        eventos.add(evento);
    }

    public List<Evento> getEventos() {
        return eventos;
    }

    public void adicionarReserva(Evento evento, String responsavel, int quantidadePessoas, String dataReserva, double valorTotal) {
        Reserva reserva = new Reserva(evento, responsavel, quantidadePessoas, dataReserva, valorTotal);
        reservas.add(reserva);
    }

    public List<Reserva> getReservas() {
        return reservas;
    }
}
