package br.edu.up;

import java.util.Scanner;

import br.edu.up.controle.Controller;
import br.edu.up.modelo.Evento;
import br.edu.up.tela.View;

public class ProgramaPrincipal {
    private Controller controller;
    private View programaView;

    public ProgramaPrincipal() {
        this.controller = new Controller();
        this.programaView = new View();
    }

    public void executar() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            programaView.exibirMenu();
            System.out.print("Escolha uma opção: ");
            String opcao = scanner.nextLine();

            switch (opcao) {
                case "1":
                    System.out.print("Nome do evento: ");
                    String nome = scanner.nextLine();
                    System.out.print("Data do evento: ");
                    String data = scanner.nextLine();
                    System.out.print("Local do evento: ");
                    String local = scanner.nextLine();
                    System.out.print("Lotação máxima do evento: ");
                    int lotacaoMaxima = Integer.parseInt(scanner.nextLine());
                    System.out.print("Quantidade de ingressos vendidos: ");
                    int ingressosVendidos = Integer.parseInt(scanner.nextLine());
                    System.out.print("Preço do ingresso: ");
                    double preco = Double.parseDouble(scanner.nextLine());

                    controller.adicionarEvento(nome, data, local, lotacaoMaxima, ingressosVendidos, preco);
                    break;

                case "2":
                    programaView.listarEventos(controller.getEventos());
                    break;

                case "3":
                    programaView.listarEventos(controller.getEventos());
                    System.out.print("Escolha o evento para reserva: ");
                    int indiceEvento = Integer.parseInt(scanner.nextLine()) - 1;
                    Evento eventoSelecionado = controller.getEventos().get(indiceEvento);
                    System.out.print("Responsável pela reserva: ");
                    String responsavel = scanner.nextLine();
                    System.out.print("Quantidade de pessoas: ");
                    int quantidadePessoas = Integer.parseInt(scanner.nextLine());
                    System.out.print("Data da reserva: ");
                    String dataReserva = scanner.nextLine();
                    double valorTotal = eventoSelecionado.getPreco() * quantidadePessoas;

                    controller.adicionarReserva(eventoSelecionado, responsavel, quantidadePessoas, dataReserva, valorTotal);
                    break;

                case "4":
                    programaView.listarReservas(controller.getReservas());
                    break;

                case "0":
                    System.out.println("Saindo...");
                    scanner.close();
                    return;

                default:
                    System.out.println("Opção inválida. Tente novamente.");
            }
        }
    }

    public static void main(String[] args) {
        ProgramaPrincipal programa = new ProgramaPrincipal();
        programa.executar();
    }
}
